1. Your pair development journey during this project. Focus on aspects you find noteworthy, e.g., process, pairing, testing, design decisions, refactoring, use of the repository.
        It was very useful to create our state machine model and write out our test names beforehand from the extra credit assignment. These really helped to guide us.
        It helped us make our program the best it can be by making sure tests pass and follow the design patterns we have been going over in class. Using the simplestopwatch and
        refactoring it also helped in that much of the outline was already there. Furthermore, having a repository that is easily shared between a team makes it easy 
        to collaborate and work on multiple portions at once.

2. The relationship between your state machine model from this project and your actual code. Possible talking points are:

        a. What are the similarities and differences between model and code?
            The four main states in both the code and state machine model are alarming, incrementing, running, and stopped.
            The Alarm state sets the alarm to play on tick, both in the code and shown in the model.
            During the running state, the tick is decremented until the time hits 0 and the alarm goes off.
            When there is a click during the alarm, it is reset and set back to the default stop state.
            When it is clicked after stopping, the timer increments until the time is set and it is ready to run.
        b. Is it more effective to code or model first?
            It can be effective to build the model first to get an idea of how the code should be organized.
            Next the programmer writes the code. If during implementation they make changes to the format of the code, that is okay.
            It is better to have a general outline first, but changes can still be made if they make more sense when the code is being built.
            Just make sure to adjust the model accordingly after completing the code.
        c. Now that you have the code, what, if any, changes would you make to your model?
            We would not need to make any changes to our model because it matched how our code was written. It guided
            us throughout the process and was a great way to follow and outline.