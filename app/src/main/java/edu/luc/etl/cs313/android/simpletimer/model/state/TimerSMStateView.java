package edu.luc.etl.cs313.android.simpletimer.model.state;

/**
 * The restricted view states have of their surrounding state machine.
 * This is a client-specific interface in Peter Coad's terminology.
 *
 * @author laufer
 */
interface TimerSMStateView {

    // transitions
    void toRunningState();
    void toStoppedState();
    void toIncrementingState();
    void toAlarmingState();

    // actions
    void actionInit();
    void actionReset();
    void actionStart();
    void actionStop();
    boolean actionInc();
    boolean actionDec();
    void actionUpdateView();
    void actionResetTimeToStart();
    boolean actionDecTimeToStart();

    // state-dependent UI updates
    void updateUIRuntime();

    void playAlarm();
    void playBeep();
}
