package edu.luc.etl.cs313.android.simpletimer.model.state;

import edu.luc.etl.cs313.android.simpletimer.R;

class AlarmingState implements TimerState {

    public AlarmingState(final TimerSMStateView sm) {
        this.sm = sm;
    }

    private final TimerSMStateView sm; //state machine

    @Override
    public void onClick() { //click after alarm
        sm.actionReset(); //reset time
        sm.actionStop(); //stop time
        sm.toStoppedState(); //send to stopped state
    }

    @Override
    public void onTick() {
        sm.playAlarm();
    } //play alarm sound

    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.ALARMING;
    } //alarming state
}
