package edu.luc.etl.cs313.android.simpletimer.model.state;

import edu.luc.etl.cs313.android.simpletimer.R;

class RunningState implements TimerState {

    public RunningState(final TimerSMStateView sm) {
        this.sm = sm;
    }

    private final TimerSMStateView sm;

    @Override
    public void onClick() {
        sm.actionReset(); //initial reset
        sm.actionStop();
        sm.toStoppedState();
    }

    @Override
    public void onTick() {
        if (sm.actionDec()){ //decrement time
            sm.toAlarmingState(); //when time=0, send to alarm state
        }
    }

    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.RUNNING;
    } //running state
}
