package edu.luc.etl.cs313.android.simpletimer.model.time;

import static edu.luc.etl.cs313.android.simpletimer.common.Constants.*;

/**
 * An implementation of the stopwatch data model.
 */
public class DefaultTimeModel implements TimeModel {

    private int runningTime = 0;
    private int timeToStart = TIME_UNTIL_START;

    @Override
    public void resetRuntime() {
        runningTime = 0;
    }

    @Override
    public void incRuntime() {
        runningTime += 1;
    }

    @Override
    public void decRuntime() { runningTime -= 1; }

    @Override
    public int getRuntime() {
        return runningTime;
    }

    @Override
    public void resetTimeToStart(){timeToStart = TIME_UNTIL_START;}

    @Override
    public void decTimeToStart(){timeToStart -= 1;}

    @Override
    public boolean timeToStartIsZero(){return timeToStart == 0;}

    @Override
    public boolean runtimeIsZero(){return runningTime == 0;}

    @Override
    public boolean runtimeIsMax(){return runningTime == MAX_RUNTIME;}
}