package edu.luc.etl.cs313.android.simpletimer.test.model.time;


import static edu.luc.etl.cs313.android.simpletimer.common.Constants.SEC_PER_MIN;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import edu.luc.etl.cs313.android.simpletimer.model.time.TimeModel;

/**
 * Testcase superclass for the time model abstraction.
 * This is a simple unit test of an object without dependencies.
 *
 * @author laufer
 * @see http://xunitpatterns.com/Testcase%20Superclass.html
 */
public abstract class AbstractTimeModelTest {

    private TimeModel model;

    /**
     * Setter for dependency injection. Usually invoked by concrete testcase
     * subclass.
     *
     * @param model
     */
    protected void setModel(final TimeModel model) {
        this.model = model;
    }

    /**
     * Verifies that runtime is initially 0 or less.
     */
    @Test
    public void testPreconditions() {
        assertEquals(0, model.getRuntime());
    }

    /**
     * Verifies that runtime is incremented correctly.
     */
    @Test
    public void testIncrementRuntimeOne() {
        final int rt = model.getRuntime();
        model.incRuntime();
        assertEquals(rt + 1, model.getRuntime());
    }

    /**
     * Verifies that runtime increments correctly with multiple inc.
     */
    @Test
    public void testIncrementRuntimeMany() {
        for (int i = 0; i < SEC_PER_MIN; i ++) {
            model.incRuntime();
        }
        assertEquals(60, model.getRuntime());
    }
}
