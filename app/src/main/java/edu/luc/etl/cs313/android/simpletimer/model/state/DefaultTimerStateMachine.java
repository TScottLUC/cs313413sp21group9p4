package edu.luc.etl.cs313.android.simpletimer.model.state;

import edu.luc.etl.cs313.android.simpletimer.common.TimerUIUpdateListener;
import edu.luc.etl.cs313.android.simpletimer.model.clock.ClockModel;
import edu.luc.etl.cs313.android.simpletimer.model.time.TimeModel;

/**
 * An implementation of the state machine for the stopwatch.
 *
 * @author laufer
 */
public class DefaultTimerStateMachine implements TimerStateMachine {

    public DefaultTimerStateMachine(final TimeModel timeModel, final ClockModel clockModel) {
        this.timeModel = timeModel;
        this.clockModel = clockModel;
    }

    private final TimeModel timeModel;

    private final ClockModel clockModel;

    /**
     * The internal state of this adapter component. Required for the State pattern.
     */
    private TimerState state;

    protected void setState(final TimerState state) {
        this.state = state;
        uiUpdateListener.updateState(state.getId());
    }

    private TimerUIUpdateListener uiUpdateListener;

    @Override
    public void setUIUpdateListener(final TimerUIUpdateListener uiUpdateListener) {
        this.uiUpdateListener = uiUpdateListener;
    }

    // forward event uiUpdateListener methods to the current state
    // these must be synchronized because events can come from the
    // UI thread or the timer thread
    @Override
    public synchronized void onClick() {
        state.onClick();
    }

    @Override
    public synchronized void onTick() {
        state.onTick();
    }

    @Override
    public void updateUIRuntime() {
        uiUpdateListener.updateTime(timeModel.getRuntime());
    }

    @Override
    public void playAlarm() {
        uiUpdateListener.playAlarm();
    }

    @Override
    public void playBeep() { uiUpdateListener.playBeep(); }

    // known states
    private final TimerState STOPPED = new StoppedState(this);
    private final TimerState RUNNING = new RunningState(this);
    private final TimerState INCREMENTING = new IncrementingState(this);
    private final TimerState ALARMING = new AlarmingState(this);

    // transitions
    @Override
    public void toRunningState() {
        setState(RUNNING);
    }

    @Override
    public void toStoppedState() {
        setState(STOPPED);
    }

    @Override
    public void toIncrementingState() {
        setState(INCREMENTING);
    }

    @Override
    public void toAlarmingState() {
        setState(ALARMING);
    }

    // actions
    @Override
    public void actionInit() {
        toStoppedState();
        actionReset();
    }

    @Override
    public void actionReset() {
        timeModel.resetRuntime();
        actionUpdateView();
    }

    @Override
    public void actionStart() {
        clockModel.start();
    }

    @Override
    public void actionStop() {
        clockModel.stop();
    }

    @Override
    public boolean actionInc() {
        timeModel.incRuntime();
        actionUpdateView();
        return timeModel.runtimeIsMax();
    }

    @Override
    public boolean actionDec() {
        timeModel.decRuntime();
        actionUpdateView();
        return timeModel.runtimeIsZero();
    }

    @Override
    public void actionUpdateView() {
        state.updateView();
    }

    @Override
    public boolean actionDecTimeToStart() {
        timeModel.decTimeToStart();
        return timeModel.timeToStartIsZero();
    }

    @Override
    public void actionResetTimeToStart() {
        timeModel.resetTimeToStart();
    }
}
