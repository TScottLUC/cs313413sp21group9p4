package edu.luc.etl.cs313.android.simpletimer.model.state;

import edu.luc.etl.cs313.android.simpletimer.R;

class StoppedState implements TimerState {

    public StoppedState(final TimerSMStateView sm) {
        this.sm = sm;
    }

    private final TimerSMStateView sm; //state machine

    @Override
    public void onClick() {
        sm.actionInc(); //begin incrementing
        sm.actionResetTimeToStart(); //total time to run
        sm.actionStart(); //start
        sm.toIncrementingState(); //increment down until alarm
    }

    @Override
    public void onTick() {
        throw new UnsupportedOperationException("onTick");
    } //no tick when stopped

    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.STOPPED;
    } //stopped state
}
