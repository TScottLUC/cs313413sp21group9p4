package edu.luc.etl.cs313.android.simpletimer.android;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import java.io.IOException;

import edu.luc.etl.cs313.android.simpletimer.R;
import edu.luc.etl.cs313.android.simpletimer.common.TimerUIUpdateListener;
import edu.luc.etl.cs313.android.simpletimer.model.ConcreteTimerModelFacade;
import edu.luc.etl.cs313.android.simpletimer.model.TimerModelFacade;

/**
 * A thin adapter component for the stopwatch.
 *
 * @author laufer
 */
public class TimerAdapter extends Activity implements TimerUIUpdateListener {

    private static String TAG = "timer-android-activity";

    /**
     * The state-based dynamic model.
     */
    private TimerModelFacade model;

    protected void setModel(final TimerModelFacade model) {
        this.model = model;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // inject dependency on view so this adapter receives UI events
        setContentView(R.layout.activity_main);
        // inject dependency on model into this so model receives UI events
        this.setModel(new ConcreteTimerModelFacade());
        // inject dependency on this into model to register for UI updates
        model.setUIUpdateListener(this);
        if (savedInstanceState == null) {
            restoreModelFromPrefs();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        model.onStart();
    }


    @Override
    protected void onStop() {
        super.onStop();
        model.onStop();
        saveModelToPrefs();
    }

    @Override
    protected void onResume() {
        super.onResume();
        model.onResume();
    }

    /** Plays the alarm sound (for the alarming state). */
    public void playAlarm() {
        runOnUiThread(() -> {
            final Uri defaultRingtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
            final MediaPlayer mediaPlayer = new MediaPlayer();
            final Context context = getApplicationContext();

            try {
                mediaPlayer.setDataSource(context, defaultRingtoneUri);
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
                mediaPlayer.prepare();
                mediaPlayer.setOnCompletionListener(MediaPlayer::release);
                mediaPlayer.start();
            } catch (final IOException ex) {
                throw new RuntimeException(ex);
            }
        });
    }

    /**Plays the beep sound (for the transition from incrementing to running). */
    public void playBeep() {
        runOnUiThread(() -> {
            final Uri defaultRingtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            final MediaPlayer mediaPlayer = new MediaPlayer();
            final Context context = getApplicationContext();

            try {
                mediaPlayer.setDataSource(context, defaultRingtoneUri);
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
                mediaPlayer.prepare();
                mediaPlayer.setOnCompletionListener(MediaPlayer::release);
                mediaPlayer.start();
            } catch (final IOException ex) {
                throw new RuntimeException(ex);
            }
        });
    }

    /**
     * Updates the seconds and minutes in the UI.
     * @param time
     */
    public void updateTime(final int time) {
        // UI adapter responsibility to schedule incoming events on UI thread
        runOnUiThread(() -> {
            final TextView tvS = (TextView) findViewById(R.id.seconds);
            final int seconds = time;
            tvS.setText(Integer.toString(seconds / 10) + Integer.toString(seconds % 10));
        });
    }

    /**
     * Updates the state name in the UI.
     * @param stateId
     */
    public void updateState(final int stateId) {
        // UI adapter responsibility to schedule incoming events on UI thread
        runOnUiThread(() -> {
            final TextView stateName = (TextView) findViewById(R.id.stateName);
            stateName.setText(getString(stateId));
        });
    }

    // forward event listener methods to the model
    public void onClick(final View view) {
        model.onClick();
    }

    protected void restoreModelFromPrefs() {
        final SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        final int value = sharedPref.getInt(getString(R.string.seconds), model.getRuntime());
        for (int i = model.getRuntime();i<value;i++){
            model.incRuntime();
        }
    }

    protected void saveModelToPrefs() {
        final SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(getString(R.string.seconds), model.getRuntime());
        editor.commit();
    }

}
