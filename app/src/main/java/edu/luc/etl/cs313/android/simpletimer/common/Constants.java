package edu.luc.etl.cs313.android.simpletimer.common;

/**
 * Constants for the time calculations used by the timer.
 */
public final class Constants {

    public static int SEC_PER_TICK = 1;
    public static int SEC_PER_MIN  = 60;

    public static int MAX_RUNTIME = 99;
    public static int TIME_UNTIL_START = 3;

    private Constants() { }
}