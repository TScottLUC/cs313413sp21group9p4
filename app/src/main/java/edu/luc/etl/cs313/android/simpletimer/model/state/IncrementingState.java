package edu.luc.etl.cs313.android.simpletimer.model.state;

import edu.luc.etl.cs313.android.simpletimer.R;

class IncrementingState implements TimerState {

    public IncrementingState(final TimerSMStateView sm) {
        this.sm = sm;
    }

    private final TimerSMStateView sm; //state machine

    @Override
    public void onClick() {
        sm.actionResetTimeToStart(); //initial restart time
        if (sm.actionInc()){ //increment time
            sm.playBeep(); //beep
            sm.toRunningState(); //send to running state
        }
    }

    @Override
    public void onTick() {
       if (sm.actionDecTimeToStart()) { //decrement time
           sm.playBeep(); //play beep
           sm.toRunningState(); //send to running state
       }
    }

    @Override
    public void updateView() {
       sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.INCREMENTING;
    } //incrementing state
}
