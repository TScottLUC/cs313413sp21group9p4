package edu.luc.etl.cs313.android.simpletimer.model.time;

/**
 * The passive data model of the stopwatch.
 * It does not emit any events.
 *
 * @author laufer
 */
public interface TimeModel {
    void resetRuntime();
    void incRuntime();
    void decRuntime();
    int getRuntime();
    void resetTimeToStart();
    void decTimeToStart();
    boolean timeToStartIsZero();
    boolean runtimeIsZero();
    boolean runtimeIsMax();
}
