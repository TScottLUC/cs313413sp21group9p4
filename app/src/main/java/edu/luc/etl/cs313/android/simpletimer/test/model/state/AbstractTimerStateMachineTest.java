package edu.luc.etl.cs313.android.simpletimer.test.model.state;

import static edu.luc.etl.cs313.android.simpletimer.common.Constants.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Timer;

import edu.luc.etl.cs313.android.simpletimer.R;
import edu.luc.etl.cs313.android.simpletimer.common.TimerUIUpdateListener;
import edu.luc.etl.cs313.android.simpletimer.model.clock.ClockModel;
import edu.luc.etl.cs313.android.simpletimer.model.clock.OnTickListener;
import edu.luc.etl.cs313.android.simpletimer.model.state.TimerStateMachine;
import edu.luc.etl.cs313.android.simpletimer.model.time.TimeModel;

/**
 * Testcase superclass for the stopwatch state machine model. Unit-tests the state
 * machine in fast-forward mode by directly triggering successive tick events
 * without the presence of a pseudo-real-time clock. Uses a single unified mock
 * object for all dependencies of the state machine model.
 *
 * @author laufer
 * @see http://xunitpatterns.com/Testcase%20Superclass.html
 */
public abstract class AbstractTimerStateMachineTest {

    private TimerStateMachine model;

    private UnifiedMockDependency dependency;

    @Before
    public void setUp() throws Exception {
        dependency = new UnifiedMockDependency();
    }

    @After
    public void tearDown() {
        dependency = null;
    }

    /**
     * Setter for dependency injection. Usually invoked by concrete testcase
     * subclass.
     *
     * @param model
     */
    protected void setModel(final TimerStateMachine model) {
        this.model = model;
        if (model == null)
            return;
        this.model.setUIUpdateListener(dependency);
        this.model.actionInit();
    }

    protected UnifiedMockDependency getDependency() {
        return dependency;
    }

    /**
     * Verifies that we're initially in the stopped state (and told the listener
     * about it).
     */
    @Test
    public void testPreconditions() {
        assertEquals(R.string.STOPPED, dependency.getState());
    }

    /**
     * Verifies the following scenario: time is 0, click,
     * expect time 1 and state incrementing.
     */
    @Test
    public void testScenarioIncrement() {
        assertTimeEquals(0);
        // directly invoke the button press event handler methods
        model.onClick();
        assertTimeEquals(1);
        assertEquals(R.string.INCREMENTING,dependency.getState());
    }

    /**
     * Verifies the following scenario: time is 0, click 10 times,
     * expect time 10 and incrementing state, tick 5 times, expect time 8
     * and running state.
     */
    @Test
    public void testTickWhileRunning(){
        assertTimeEquals(0);
        onClickRepeat(10);
        assertTimeEquals(10);
        assertEquals(R.string.INCREMENTING,dependency.getState());
        onTickRepeat(5);
        assertTimeEquals(8);
        assertEquals(R.string.RUNNING,dependency.getState());
    }

    /**
     * Verifies the following scenario: time is 0, click 5 times,
     * expect time 5 and incrementing state, tick 3 times,
     * expect time 5 and running state, tick 3 times, expect time 2
     * and running state, tick 2 times,
     * expect time 0 and alarming state.
     */
    @Test
    public void testScenarioRunningToAlarm(){
        assertTimeEquals(0);
        onClickRepeat(5);
        assertTimeEquals(5);
        assertEquals(R.string.INCREMENTING,dependency.getState());
        onTickRepeat(3);
        assertTimeEquals(5);
        assertEquals(R.string.RUNNING, dependency.getState());
        onTickRepeat(3);
        assertTimeEquals(2);
        assertEquals(R.string.RUNNING,dependency.getState());
        onTickRepeat(2);
        assertTimeEquals(0);
        assertEquals(R.string.ALARMING, dependency.getState());
    }


    /**
     * Sends the given number of tick events to the model.
     *
     *  @param n the number of tick events
     */
    protected void onTickRepeat(final int n) {
        for (int i = 0; i < n; i++)
            model.onTick();
    }

    /**
     * Send the given number of click events to the model.
     * @param n the number of click events
     */
    protected void onClickRepeat(final int n){
        for (int i = 0; i < n;i++){
            model.onClick();
        }
    }

    /**
     * Checks whether the model has invoked the expected time-keeping
     * methods on the mock object.
     */
    protected void assertTimeEquals(final int t) {
        assertEquals(t, dependency.getTime());
    }
}

/**
 * Manually implemented mock object that unifies the three dependencies of the
 * stopwatch state machine model. The three dependencies correspond to the three
 * interfaces this mock object implements.
 *
 * @author laufer
 */
class UnifiedMockDependency implements TimeModel, ClockModel, TimerUIUpdateListener {

    private int timeValue = -1, stateId = -1;

    private Timer timer;

    private int runningTime = 0;

    private int timeToStart = TIME_UNTIL_START;

    private boolean started = false;

    public int getTime() {
        return timeValue;
    }

    public int getState() {
        return stateId;
    }

    public boolean isStarted() {
        return started;
    }

    @Override
    public void resetTimeToStart(){
        timeToStart = TIME_UNTIL_START;
    }

    @Override
    public void decTimeToStart(){
        timeToStart -= 1;
    }

    @Override
    public boolean timeToStartIsZero(){
        return timeToStart == 0;
    }

    @Override
    public boolean runtimeIsZero(){
        return runningTime == 0;
    }

    @Override
    public boolean runtimeIsMax() { return runningTime == MAX_RUNTIME;}

    @Override
    public void decRuntime(){
        runningTime -= 1;
    }

    @Override
    public void updateTime(final int timeValue) {
        this.timeValue = timeValue;
    }

    @Override
    public void updateState(final int stateId) {
        this.stateId = stateId;
    }

    @Override
    public void setOnTickListener(OnTickListener listener) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void playBeep(){ }

    @Override
    public void playAlarm(){ }

    @Override
    public void start() {
        started = true;
    }

    @Override
    public void stop() {
        started = false;
    }

    public void resetRuntime() {
        runningTime = 0;
    }

    @Override
    public void resetTimeout(){
        if (null != timer) {
            stop();
        }
        start();
    }

    @Override
    public void incRuntime() {
        runningTime++;
    }

    @Override
    public int getRuntime() {
        return runningTime;
    }

}
