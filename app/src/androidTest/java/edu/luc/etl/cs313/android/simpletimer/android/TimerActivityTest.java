package edu.luc.etl.cs313.android.simpletimer.android;

import android.support.test.filters.SmallTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import org.junit.Rule;
import org.junit.runner.RunWith;
import edu.luc.etl.cs313.android.simpletimer.test.android.AbstractTimerActivityTest;

/**
 * Concrete Android test subclass. Has to inherit from framework class and uses delegation to
 * concrete subclass of abstract test superclass.
 *
 * @author laufer
 * @see http://developer.android.com/tools/testing/activity_testing.html
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class TimerActivityTest extends AbstractTimerActivityTest {

    @Rule
    public ActivityTestRule<TimerAdapter> activityRule =
            new ActivityTestRule<>(TimerAdapter.class);

    @Override
    protected TimerAdapter getActivity() {
        return activityRule.getActivity();
    }
}
